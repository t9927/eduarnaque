from django.urls import path
from . import views

app_name = 'acceuil'
urlpatterns = [
    path('',views.index, name='index'),
    path('level/',views.level,name='level'),
    path('niveau/',views.niveau ,name='niveau'),

]

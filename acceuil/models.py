from django.db import models

# Create your models here.
class Level(models.Model):
    niv= {
        ('facile','facile'),
        ('moyen','moyen'),
        ('difficile','difficile'),

    }
    niveau = models.CharField(("niveau") ,choices=niv, max_length=50, default="facile")

    def __str__(self):
        return self.niveau
    

class Scenario(models.Model):
    niveau_sc = models.ForeignKey("Level", on_delete=models.CASCADE)
    titre = models.CharField(("titre"), max_length=50)
    img = models.ImageField(("img"),upload_to ='scenaio_img'  ,null= True, blank=True)
    description = models.TextField(('description'))

class Stage(models.Model):
    sc_stage = models.ForeignKey("Scenario", on_delete=models.CASCADE)
    img = models.ImageField(("img"), upload_to='stage_img',null= True, blank=True)
    titre = models.CharField(("titreS"), max_length=50)
    contenu = models.TextField(("contenu"))

class Quiz(models.Model):
    stage = models.ForeignKey("Stage" ,on_delete=models.CASCADE)
    question = models.CharField(("question"),max_length=50)
    res1 = models.CharField(("res1"),max_length=50)
    res2 = models.CharField(("res2"),max_length=50)
    res3 = models.CharField(("res3"),max_length=50)
    res4 = models.CharField(("res4"),max_length=50)
    reponse = models.CharField(("reponse"),max_length=50)

   
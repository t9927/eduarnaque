from django.contrib import admin
from .models import Level,Scenario,Stage,Quiz
# Register your models here.

admin.site.register(Level)
admin.site.register(Scenario)
admin.site.register(Stage)
admin.site.register(Quiz)

admin.site.site_header = 'Eduarnaque Administration'
admin.site.site_title = 'Eduarnaque'
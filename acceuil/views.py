from django.shortcuts import render
from django.http import HttpResponse
from django import forms
from .forms import LevelForm
from .models import Level
from compte.forms import loginForm

# Create your views here.
def index(request):
    return render(request, 'acceuil/index.html')

def level(request):

    niv = Level.objects.all()
    form = LevelForm()
    con = loginForm()
    context = {
        'niv': niv,
        'con': con,
        'form': form,
        'niveau': request.GET.get('niveau')
    }
    return render(request, 'acceuil/level.html', context)

def niveau(request):
    return render(request, 'acceuil/niveau.html')
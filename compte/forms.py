from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

class loginForm(AuthenticationForm):
    
        username = forms.CharField(label='username', widget=forms.TextInput(attrs={
            'class' : 'form-control',
        }))
        password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
            'class' : 'form-control',
        }))
        class Meta:
            model = User
            fields = ('username','password')

class inscriptionForm(UserCreationForm):
    username = forms.CharField(label='username', widget=forms.TextInput(attrs={
            'class' : 'form-control',
    }))
    email = forms.CharField(label='e-mail', widget=forms.EmailInput(attrs={
            'class' : 'form-control',
    }))
    fist_name = forms.CharField(label='fist name', widget=forms.TextInput(attrs={
            'class' : 'form-control',
    }))
    last_name = forms.CharField(label='last name', widget=forms.TextInput(attrs={
            'class' : 'form-control',
    }))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
            'class' : 'form-control',
    }))
    password2 = forms.CharField(label='Password comfirmation', widget=forms.PasswordInput(attrs={
            'class' : 'form-control',
    }))
    class Meta:
        model = User
        fields = ('username','email','fist_name','last_name','password1','password2')


from django.shortcuts import render,redirect
from .forms import inscriptionForm
from django.contrib.auth.forms import UserCreationForm
# Create your views here.

# inscriptiom
def inscription(request):
    if request.user.is_authenticated:
        rdirect('acceuil:index')
    if request.method == 'POST':
        form = inscriptionForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = inscriptionForm()

    context = {
        'form': form,
    }
    return render(request, 'compte/inscription.html',context)

# connexion
def login(request):
    context = {
        'form': form,
    }
    return render(request, 'compte/login.html',context)
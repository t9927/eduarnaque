from django.urls import path

from django.contrib.auth import views as auth_views

from . import views

from .forms import loginForm

app_name = 'compte'
urlpatterns = [
    path('inscription/', views.inscription, name='inscription'), 
    path('login/',auth_views.LoginView.as_view(template_name ='compte/login.html', redirect_authenticated_user=True, authentication_form = loginForm), name='login'),
    path('logout/',auth_views.LogoutView.as_view(template_name ='compte/logout.html',next_page='/compte/login'), name='logout'),
    
]

from django.shortcuts import render
from acceuil.models import Scenario, Stage,Level,Quiz

# Create your views here.
def liste(request, id):
    niv = Level.objects.all()
    sc = Scenario.objects.filter(niveau_sc = id)

    context = {
        'niv': niv,
        'sc': sc,
    }

    return render(request, 'game/liste.html', context)


# etape
def etape(request, sc_stage):
    stage = Stage.objects.filter(sc_stage = sc_stage)
    sc = Scenario.objects.all()

    context = {
        'stage': stage,
        'sc': sc,
    }
    return render(request, 'game/etape.html' ,context)

def detail(request, id):
    stage = Stage.objects.get(id=id)
    context = {
        'stage': stage,
    }
    return render(request, 'game/detail.html' ,context)


def questions(request):

    qu = Quiz.objects.all()
    context = {
        'qu':qu
    }
    return render(request, 'game/questionnaire.html', context)

def continu(request):
    return render(request, 'game/continue.html')
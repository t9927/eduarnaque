from django.urls import path
from . import views

app_name = 'game'
urlpatterns = [
    path('liste/<int:id>/', views.liste, name='liste' ),
    path('etape/<int:sc_stage>/',views.etape, name='etape'),
    path('detail/<int:id>/',views.detail, name='detail'),
    path('questions/',views.questions, name='questions'),
    path('continu/', views.continu, name='continu' ),
    ]
